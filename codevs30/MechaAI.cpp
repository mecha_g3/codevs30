//
//  MechaAI.cpp
//  codevs30
//
//  Created by shingo on 2013/11/01.
//  Copyright (c) 2013年 shingo. All rights reserved.
//

#include "MechaAI.h"

#include<iostream>
#include<vector>
#include<string>
#include<algorithm>
using namespace std;

MechaAI::MechaAI(){
    depth = 40;
    friend_ans = false;
    last_ans.resize(2);
}

void MechaAI::calc_firstok(){
    firstok = vector<vector<bool> >(H, vector<bool>(W, true));

    for(int y = 0; y < H; ++y){
        for(int x = 0; x < W; ++x){
            if (field[y][x] == '#' || field[y][x] == '+'){
                firstok[y][x] = false;
            }
        }
    }
    
    for(int i = 0; i < bombs.size(); i++){
        if(! bombs[i].is_virtual){
            firstok[bombs[i].y][bombs[i].x] = false;
        }
    }
}

void MechaAI::calc_feature_field(bool enable_virtual_bomb = false){
    feature_field = vector<vector<vector<int> > > (depth, vector<vector<int> >(H, vector<int>(W, 0)));

    for(int y = 0; y < H; y++){
        for(int x = 0; x < W; x++){
            if(field[y][x] == '#'){
                feature_field[0][y][x] = 2;
            }
            else if(field[y][x] == '+'){
                feature_field[0][y][x] = 1;
            }
            else{
                feature_field[0][y][x] = 0;
            }
        }
    }

    for(int i = 0; i < bombs.size(); i++){
        if(! bombs[i].is_virtual || enable_virtual_bomb){
            feature_field[0][bombs[i].y][bombs[i].x] = 1;
        }
    }

    vector<bool> checked(bombs.size(), false);
    for (int t = 0; t < depth; t++){
        bool fire = true;
        while(fire){
            fire = false;
            for(int i = 0; i < bombs.size(); i++){
                const Bomb& bomb = bombs[i];

                if(! bomb.is_virtual || enable_virtual_bomb){
                    if (! checked[i]){
                        if((bomb.turn == t) || (feature_field[t][bomb.y][bomb.x] >= 4)){
                            checked[i] = true;
                            fire = true;
                            feature_field[t][bomb.y][bomb.x] = 4;

                            for(int k = 0; k < 4; ++k){
                                for (int p = 1; p <= bomb.pow; ++p) {
                                    const int y = bomb.y + dy[k] * p;
                                    const int x = bomb.x + dx[k] * p;
                                    if((feature_field[t][y][x] == 2) || (feature_field[t][y][x] == 5))
                                        break;
                                    if (feature_field[t][y][x] == 1){
                                        feature_field[t][y][x] = 5;
                                        break;
                                    }
                                    feature_field[t][y][x] = 4;
                                }
                            }
                        }
                    }
                }
            }
        }

        if (t != depth-1){
            for (int y = 0; y < H; ++y){
                for (int x = 0; x < W; ++x){
                    if (feature_field[t][y][x] == 2){
                        feature_field[(t + 1)][y][x] = 2;
                    }
                    else if (feature_field[t][y][x] == 1){
                        feature_field[(t + 1)][y][x] = 1;
                    }
                    else{
                        feature_field[(t + 1)][y][x] = 0;
                    }
                }
            }
        }
    }
}

int MechaAI::get_alive_turn(const Character& chara, int move_dir){
    vector<vector<vector<int> > > tmp_field(depth+1, vector<vector<int> >(H, vector<int>(W, 0)));

    for(int t = 0; t < depth; t++){
        if (t == 0){
            for (int k = 0; k < 5; k++){
                if ((move_dir >> k) % 2 != 0){
                    const int y = chara.y + dy[k];
                    const int x = chara.x + dx[k];
                    tmp_field[0][y][x] |= 1 << k;
                }
            }
        }
        else{
            for(int y = 0; y < H; y++){
                for (int x = 0; x < W; x++){
                    if (tmp_field[(t - 1)][y][x] != 0){
                        for (int k = 0; k < 5; k++) {
                            const int ny = y + dy[k];
                            const int nx = x + dx[k];
                            tmp_field[t][ny][nx] |= tmp_field[(t - 1)][y][x];
                        }
                    }
                }
            }
        }

        bool alive = false;
        for (int y = 0; y < H; y++) {
            for (int x = 0; x < W; x++) {
                if ((t != 0) || (feature_field[t][y][x] != 1) || (! firstok[y][x])){
                    if (feature_field[t][y][x] != 0) {
                        if (feature_field[t][y][x] != 1) {
                            tmp_field[t][y][x] = 0;
                        }
                        else if (t != 0){
                            tmp_field[t][y][x] &= tmp_field[(t - 1)][y][x];
                        }
                        else{
                            tmp_field[t][y][x] &= 16;
                        }
                    }
                }
                alive |= !!tmp_field[t][y][x];
            }
        }

        if (! alive){
            return t;
        }
    }
    return depth;
}

vector<vector<int> > MechaAI::getset(int char_index, int bomb_delay){
    vector<vector<int> > ret(5, vector<int>(4));
    const Character& chara = m_chars[char_index];
    const bool use_bomb = bomb_delay > 0;

    for (int k = 0; k < 5; k++) {
        int y = chara.y + dy[k];
        int x = chara.x + dx[k];
        if ((! firstok[y][x]) && (k != 4)) {
            ret[k][0] = 0;
            ret[k][1] = depth;
            ret[k][2] = 0;
            ret[k][3] = depth;
        }
        else{
            if(use_bomb){
                Bomb bomb;
                bomb.cid = chara.cid;
                bomb.y = y;
                bomb.x = x;
                bomb.turn = bomb_delay;
                bomb.pow = chara.pow;
                bomb.is_virtual = false;
                bombs.push_back(bomb);
            }

            calc_feature_field(false);

            int tempet = depth;
            int tempmt = depth;

            //mID[mcount] = p; ????

            for (int i = 0; i < m_chars.size(); i++) {
                if(chara.cid == m_chars[i].cid){
                    tempmt = min(tempmt, get_alive_turn(m_chars[i], 1 << k));
                }
                else if(friend_ans){
                    tempmt = min(tempmt, get_alive_turn(m_chars[i], 1 << friend_move_index));
                }
                else{
                    tempmt = min(tempmt, get_alive_turn(m_chars[i], 31));
                }
            }

            for (int i = 0; i < o_chars.size(); i++){
                tempet = min(tempet, get_alive_turn(o_chars[i], 31));
            }

            ret[k][0] = tempmt;
            ret[k][1] = tempet;

            if(use_bomb){
                bombs.pop_back();
            }
        }
    }

    for (int k = 0; k < 5; k++) {
        int y = chara.y + dy[k];
        int x = chara.x + dx[k];
        if ((! firstok[y][x]) && (k != 4)) {
            ret[k][0] = 0;
            ret[k][1] = depth;
            ret[k][2] = 0;
            ret[k][3] = depth;
        }
        else{
            if(use_bomb){
                Bomb bomb;
                bomb.cid = chara.cid;
                bomb.y = y;
                bomb.x = x;
                bomb.turn = bomb_delay;
                bomb.pow = chara.pow;
                bomb.is_virtual = false;
                bombs.push_back(bomb);
            }

            calc_feature_field(true);

            int tempmt = depth;
            int tempet = depth;
            
            //mID[mcount] = p; ???
            
            for (int i = 0; i < m_chars.size(); i++) {
                if(chara.cid == m_chars[i].cid){
                    tempmt = min(tempmt, get_alive_turn(m_chars[i], 1 << k));
                }
                else if(friend_ans){
                    tempmt = min(tempmt, get_alive_turn(m_chars[i], 1 << friend_move_index));
                }
                else{
                    tempmt = min(tempmt, get_alive_turn(m_chars[i], 31));
                }
            }
            
            for (int i = 0; i < o_chars.size(); i++){
                tempet = min(tempet, get_alive_turn(o_chars[i], 31));
            }
            
            ret[k][2] = tempmt;
            ret[k][3] = tempet;
            
            if(use_bomb){
                bombs.pop_back();
            }
        }
    }
    return ret;
}


vector<Answer> MechaAI::solve(){
    calc_firstok();

    for (int i = 0; i < o_chars.size(); i++) {
        const Character& chara = o_chars[i];
        for (int j = 0; j < 5; j++){
            const int y = chara.y + dy[j];
            const int x = chara.x + dx[j];
            if(firstok[y][x]){
                Bomb bomb;
                bomb.cid = chara.cid;
                bomb.y = y;
                bomb.x = x;
                bomb.turn = 5;
                bomb.pow = chara.pow;
                bomb.is_virtual = true;
                bombs.push_back(bomb);

                if(j){
                  const int ny = y + dy[j];
                  const int nx = x + dx[j];
                  if(firstok[ny][nx]){
                    Bomb bomb;
                    bomb.cid = chara.cid;
                    bomb.y = ny;
                    bomb.x = nx;
                    bomb.turn = 5;
                    bomb.pow = chara.pow;
                    bomb.is_virtual = true;
                    bombs.push_back(bomb);
                  }
                }
            }
        }
    }

    vector<Answer> ret(m_chars.size());

    for (int i = 0; i < m_chars.size(); i++) {
        const Character& chara = m_chars[i];

        vector<vector<vector<int> > > check(5);
        int bomb_delay[2][5] = {
            { 0, 7, 7, 7, 7},
            { 0, 7, 7, 7, 7}
        };
        
        for (int j = 0; j < 5; j++) {
            check[j] = getset(i, bomb_delay[i][j]);
        }

        vector<int> ar(25);
        for (int j = 0; j < ar.size(); j++){
            ar[j] = j;
        }
        random_shuffle(ar.begin(), ar.end());
       
        int best = 1;
        long long bestscore = -999999999999999999LL;

        for (int j = 0; j < ar.size(); j++) {
            int a = ar[j] / 5;
            int b = ar[j] % 5;
            if((a == 0) || (chara.rem > 0)){
                long long score = (check[a][b][0] - check[a][b][1]) * 10000000000000LL;
                if ((check[a][b][0] == check[a][b][1]) && (check[a][b][0] != depth)){
                    score -= 5000000000000LL;
                }

                score += check[a][b][2] * 100000LL;

                int y = chara.y + dy[b];
                int x = chara.x + dx[b];
                
                for (int k = 0; k < items.size(); ++k) {
                    score += 50000LL / (abs(items[k].y - y) + abs(items[k].x - x) + 1);
                }
                
                for (int k = 0; k < o_chars.size(); ++k) {
                    score -= 1000LL * (abs(o_chars[k].y - y) + abs(o_chars[k].x - x));
                }
                
                for (int k = 0; k < m_chars.size(); ++k) {
                    int cy = m_chars[k].y;
                    int cx = m_chars[k].x;
                    if(friend_ans){
                        cy += dy[friend_move_index];
                        cx += dx[friend_move_index];
                    }
                    score -= 10000LL / (abs(y - cy) + abs(x - cx) + 1);
                }
                
                if(last_ans[i].dir != 4 && last_ans[i].dir == b){
                    //score += 5000LL;
                }
                
                if (b == 4) {
                    //score -= 10000LL;
                }
                if (a >= 1) {
                    if ((y % 2 == 1) && (x % 2 == 1) && (b != 4)) {
                        score += 100000LL;
                    }
                    else {
                        score -= 100000LL;
                        if (last_ans[i].delay > 0) {
                            score -= 100000LL;
                        }
                    }
                }
                if (score > bestscore) {
                    bestscore = score;
                    best = ar[j];
                }
            }
        }

        Answer ans;
        ans.dir = best % 5;
        ans.delay = bomb_delay[i][best / 5];

        if(ans.delay > 0){
            Bomb bomb;
            bomb.cid = chara.cid;
            bomb.y = chara.y + dy[ans.dir];
            bomb.x = chara.x + dx[ans.dir];
            bomb.turn = ans.delay;
            bomb.pow = chara.pow;
            bomb.is_virtual = false;
            bombs.push_back(bomb);
            
            m_chars[i].rem--;
        }
        if(i==0){
            friend_ans = true;
            friend_move_index = ans.dir;
            friend_bomb_delay = ans.delay;
        }
        else{
            friend_ans = false;
        }
        ret[i] = ans;
    }

    last_ans = ret;
    return ret;
}