#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import os
import copy
import random
random.seed(0)

class Character:
    def __init__(self):
        self.pid = None
        self.id  = None
        self.y   = None
        self.x   = None
        self.pow = None
        self.hav = None
        self.rem = None

class Bomb:
    def __init__(self):
        self.cid  = None
        self.y    = None
        self.x    = None
        self.turn = None
        self.pow  = None

class Item:
    def __init__(self):
        self.type = None
        self.y    = None
        self.x    = None

class Answer:
    def __init__(self):
        self.cid   = None
        self.score = None
        self.move  = None
        self.bomb  = None
        self.delay = None
        self.f_field = None

class Game:
    def __init__(self):
        self.flush()

    def flush(self):
        self.turn = None
        self.MAX_TURN = None
        self.my_id = None
        self.W = None
        self.H = None
        self.char_size = None
        self.bomb_size = None
        self.item_size = None
        self.m_chars = []
        self.o_chars = []
        self.field = []
        self.bombs = []
        self.items = []

    def input(self):
        self.flush()
        self.turn = input()
        self.MAX_TURN = input()
        self.my_id = input()
        self.H ,self.W = map(int, raw_input().split())

        for y in range(self.H):
            self.field.append(raw_input().replace('@', '.'))

        self.char_size = input()
        for i in range(self.char_size):
            char = Character()
            char.pid, char.id, char.y ,char.x, char.pow, char.hav = map(int, raw_input().split())
            char.rem = char.hav
            if char.pid == self.my_id:
                self.m_chars.append(char)
            else:
                self.o_chars.append(char)

        self.bomb_size = input()
        for i in range(self.bomb_size):
            bomb = Bomb()
            bomb.cid, bomb.y ,bomb.x, bomb.turn, bomb.pow = map(int, raw_input().split())
            self.bombs.append(bomb)

            for char in self.m_chars:
                if char.id == bomb.cid:
                    char.rem -= 1

            for char in self.o_chars:
                if char.id == bomb.cid:
                    char.rem -= 1

        self.item_size = input()
        for i in range(self.item_size):
            item = Item()
            line = raw_input().split()
            if line[0] == 'NUMBER_UP':
                item.type = 0
            else:
                item.type = 1
            item.y = int(line[1])
            item.x = int(line[2])
            self.items.append(item)

        if raw_input() == "END":
            return True
        else:
            return False


class MechaAI:
    def __init__(self):
        self.game = Game()
        self.dir_list = [ (1,0), (0,1), (-1,0), (0,-1), (0,0) ]
        self.ans_dir_list  = [ 'RIGHT', 'DOWN', 'LEFT', 'UP', 'NONE' ]
        self.bomb_delay_list = [ 0, 5, 7, 9 ]
        self.feature_field = None
        self.last_ans_list = None

    def get_name(self):
        return 'mecha_g3'

    def input(self):
        return self.game.input()
        
    def prepare(self):
        ## １ターン先の敵の動きをすべて適用しておく
        for char in self.game.o_chars:
            for dir in self.dir_list:
                x = char.x + dir[0]
                y = char.y + dir[1]
                if self.game.field[y][x] == '.':
                    bomb = Bomb()
                    bomb.cid  = None
                    bomb.x    = x
                    bomb.y    = y
                    bomb.turn = 5
                    bomb.pow  = char.pow
                    self.game.bombs.append(bomb)

    def calc_feature_field(self, with_cheat=False):
        self.feature_field = []

        for turn in range(10):
            field = None
            if turn == 0:
                field = copy.deepcopy(self.game.field)
                for bomb in self.game.bombs:
                    if (bomb.cid or with_cheat):
                        field[bomb.y] = field[bomb.y][:bomb.x] + 'B' + field[bomb.y][bomb.x+1:]
            else:
                field = copy.deepcopy(self.feature_field[turn-1])
                for y in range(self.game.H):
                    for x in range(self.game.W):
                        if field[y][x] == '-' or field[y][x] == 'F':
                            field[y] = field[y][:x] + '.' + field[y][x+1:]

            self.feature_field.append(field)

            fire = True
            while fire: # 誘爆が終わるまでループ
                fire = False
                for bomb in self.game.bombs:
                    if (bomb.cid or with_cheat):
                        if field[bomb.y][bomb.x] == 'B' or field[bomb.y][bomb.x] == 'b' : # まだ爆発してない
                            if bomb.turn == turn or field[bomb.y][bomb.x] == 'b': # このターンで爆発or誘爆
                                field[bomb.y] = field[bomb.y][:bomb.x] + 'F' + field[bomb.y][bomb.x+1:]
                                fire = True
                                for dir in self.dir_list:
                                    for power in range(bomb.pow+1):
                                        x = bomb.x + dir[0] * power
                                        y = bomb.y + dir[1] * power
                                        # '-' はこのターンで燃えるソフトブロック
                                        if field[y][x] == '#' or field[y][x] == '-':
                                            break
                                        if field[y][x] == '+':
                                            field[y] = field[y][:x] + '-' + field[y][x+1:]
                                            break
                                        if field[y][x] == 'B' or field[y][x] == 'b':
                                            field[y] = field[y][:x] + 'b' + field[y][x+1:]
                                        else:
                                            field[y] = field[y][:x] + 'F' + field[y][x+1:]


    def calc_alive_turn(self, char, first_dir=False, first_force=False):
        empty_field = [ [ 0 for w in range(self.game.W) ] for h in range(self.game.H) ]
        before_field = None
        after_field  = copy.deepcopy(empty_field)

        for turn in range(10):
            before_field = copy.deepcopy(after_field)
            after_field  = copy.deepcopy(empty_field)

            if turn == 0:
                if first_dir:
                    x = char.x + first_dir[0]
                    y = char.y + first_dir[1]
                    if self.game.field[y][x] == '.' and self.feature_field[turn][y][x] != 'F' and (first_force or self.feature_field[turn][y][x] != 'B'):
                        after_field[y][x] = 1
                else:
                    for dir in self.dir_list:
                        x = char.x + dir[0]
                        y = char.y + dir[1]
                        if self.game.field[y][x] == '.' and self.feature_field[turn][y][x] != 'F' and (first_force or self.feature_field[turn][y][x] != 'B'):
                            after_field[y][x] = 1

            else:
                for y in range(self.game.H):
                    for x in range(self.game.W):
                        if before_field[y][x]:
                            for dir in self.dir_list:
                                px = x + dir[0]
                                py = y + dir[1]
                                if self.feature_field[turn][py][px] == '.':# or (dir[0] == 0 and dir[1] == 0):
                                    after_field[py][px] = 1

            alive = False
            for y in range(self.game.H):
                for x in range(self.game.W):
                    if after_field[y][x]:
                        alive = True
            if not alive:
                return turn

        return 10

    def solve(self):
        ans_list = []
        self.prepare()

        """
        sys.stderr.write('M' + str(self.game.m_chars) + '\n')
        sys.stderr.write('O' + str(self.game.o_chars) + '\n')
        """

        for char in self.game.m_chars:
            max_ans = Answer()
            max_ans.cid = char.id
            max_ans.move = (0,0)
            max_ans.bomb = 0
            max_ans.delay = 0
            max_ans.score = -10000000000000000000000000000000

            for dir in self.dir_list:
                for delay in self.bomb_delay_list:
                    ans_index = len(ans_list)

                    #######################################
                    ## スコア計算準備
                    #######################################

                    ans = Answer()
                    ans.cid = char.id
                    ans.move = dir
                    ans.bomb = (delay > 0)
                    ans.delay = delay
                    ans.score = 0

                    ## 相方の回答
                    friend = None
                    friend_ans = None
                    if ans_index > 0:
                        friend = self.game.m_chars[0]
                        friend_ans = ans_list[0]
                    else:
                        friend = self.game.m_chars[1]
                        

                    ## 前ターンの回答
                    last_ans = None
                    if self.last_ans_list and self.last_ans_list[ans_index]:
                        last_ans = self.last_ans_list[ans_index]

                    ## 今回の移動先のマス
                    x = char.x + dir[0]
                    y = char.y + dir[1]

                    ## 移動できない
                    if self.game.field[y][x] != '.':
                        break

                    ## ボムが足りない
                    if ans.bomb and char.rem <= 0:
                        break

                    ## ボムが置いてあった
                    bomb_flag = False
                    for bomb in self.game.bombs:
                        if bomb.cid and bomb.x == x and bomb.y == y:
                            bomb_flag = True
                    if bomb_flag:
                        break

                    ## 爆弾を置くパターンなら爆弾追加
                    if ans.bomb:
                        bomb = Bomb()
                        bomb.cid  = char.id
                        bomb.x    = x
                        bomb.y    = y
                        bomb.turn = delay
                        bomb.pow  = char.pow
                        self.game.bombs.append(bomb)

                    m_alive_turn = [ 10, 10 ]
                    o_alive_turn = 10

                    before_soft = 0
                    after_soft = 0
                    
                    for i in range(2):
                        ## 未来の盤面を作っておく
                        with_cheat = i==1
                        self.calc_feature_field(with_cheat)

                        ## 生存時間の計算 味方
                        m_alive_turn[i] = min(m_alive_turn[i] , self.calc_alive_turn(char, dir, ans.bomb))

                        if friend_ans:
                            # 相方の動きが決まっている
                            # 相方は既に動いているので停止動作で調べる
                            m_alive_turn[i]  = min(m_alive_turn[i] , self.calc_alive_turn(friend, (0,0), True))
                        else:
                            # 相方の動きが決まっていない
                            m_alive_turn[i]  = min(m_alive_turn[i] , self.calc_alive_turn(friend))

                        if not with_cheat:
                            ## 消えたソフトブロックの数で評価
                            before_soft = 0
                            for line in self.game.field:
                                before_soft += line.count('+')

                            after_soft = 0
                            for line in self.feature_field[9]:
                                after_soft += line.count('+')

                            ## 生存時間の計算 相手
                            for o in self.game.o_chars:
                                o_alive_sub = 0
                                for r in self.dir_list:
                                    ox = o.x + r[0]
                                    oy = o.y + r[1]
                                    same_move_1 = ans.bomb and ox == x and oy == y
                                    same_move_2 = friend_ans and friend_ans.bomb and friend.x == ox and friend.y == oy
                                    if same_move_1 or same_move_2:
                                        o_alive_sub = max(o_alive_sub, self.calc_alive_turn(o, r, True))
                                    else:
                                        o_alive_sub = max(o_alive_sub, self.calc_alive_turn(o, r, False))
                                o_alive_turn = min(o_alive_turn, o_alive_sub)

                    ## 上で追加した爆弾を破棄
                    if ans.bomb:
                        self.game.bombs.pop()


                    #######################################
                    ## スコア計算
                    #######################################

                    ## 生存時間による評価
                    ans.score += (m_alive_turn[0] - o_alive_turn) * 10000000000000
                    ans.score += m_alive_turn[1] * 1000000
                    if m_alive_turn[0] == o_alive_turn and m_alive_turn[0] < 10:
                        ans.score -= 1000000000000000

                    ## アイテムとの距離による評価
                    for item in self.game.items:
                        ans.score += 20000 / ( abs(x - item.x) + abs(y - item.y) + 1 )

                    ans.score += (before_soft - after_soft) * 10000

                    ## 敵との距離による評価
                    for i in range(len(self.game.o_chars)):
                        enemy = self.game.o_chars[i]
                        ans.score -= 1000 * ( abs(x - enemy.x) + abs(y - enemy.y) )

                    ## 味方との距離による評価
                    for i in range(len(self.game.m_chars)):
                        _char = self.game.m_chars[i]
                        if char.id != _char.id:
                            ans.score -= 10000 / ( abs(x - _char.x) + abs(y - _char.y) + 1)

                    ## 前ターンの回答をふまえて評価
                    if last_ans:
                        if last_ans.move == ans.move and ans.move != (0,0):
                            ans.score += 50000
                        elif last_ans.move == ans.move and ans.move == (0,0):
                            ans.score -= 10000

                    ## 爆弾設置場所による評価
                    if ans.bomb:
                        if x % 2 == 1 and y % 2 == 1:
                            ans.score += 100
                        else:
                            ans.score -= 100
                            if last_ans and last_ans.bomb:
                                ans.score -= 100000

                    ## 回答更新
                    if ans.score > max_ans.score:
                        max_ans = ans
                        max_ans.f_field = self.feature_field
            """
            sys.stderr.write('============================\n')
            sys.stderr.write('max_ans\n')
            sys.stderr.write('============================\n')
            sys.stderr.write('max_ans.cid  = ' + str(max_ans.cid) + '\n')
            sys.stderr.write('max_ans.move = ' + str(max_ans.move) + '\n')
            sys.stderr.write('max_ans.bomb = ' + str(max_ans.bomb) + '\n')
            sys.stderr.write('max_ans.delay= ' + str(max_ans.delay) + '\n')
            sys.stderr.write('max_ans.score= ' + str(max_ans.score) + '\n')
            sys.stderr.write('============================\n')
            sys.stderr.write('ALIVE\n')
            sys.stderr.write('============================\n')
            sys.stderr.write('m_alive_turn = ' + str(m_alive_turn) + '\n')
            sys.stderr.write('o_alive_turn = ' + str(o_alive_turn) + '\n')
            sys.stderr.write('============================\n')
            sys.stderr.write('FIELD\n')
            sys.stderr.write('============================\n')
            for line in self.game.field:
               sys.stderr.write(line + '\n')

            sys.stderr.write('============================\n')
            sys.stderr.write('FEATURE_FIELD\n')
            sys.stderr.write('============================\n')
            if max_ans.f_field:
                for t in range(len(max_ans.f_field)):
                    sys.stderr.write('TURN:' + str(self.game.turn + t) + '\n')
                    for line in max_ans.f_field[t]:
                        sys.stderr.write(line + '\n')
            """

            ans_list.append(max_ans)

            char.x += max_ans.move[0]
            char.y += max_ans.move[1]

            if max_ans.bomb:
                char.rem -= 1
                bomb = Bomb()
                bomb.cid  = char.id
                bomb.x    = char.x
                bomb.y    = char.y
                bomb.turn = max_ans.delay
                bomb.pow  = char.pow
                self.game.bombs.append(bomb)

        assert(len(ans_list) == 2)
        for ans in ans_list:
            out = self.ans_dir_list[ self.dir_list.index(ans.move) ]
            if ans.bomb:
                out += ' MAGIC ' + str(ans.delay)
            print out 

if __name__ == '__main__':
    ai = MechaAI()
    print ai.get_name()
    sys.stdout.flush()

    while(ai.input()):
        sys.stdout.flush()
        ai.solve()
        sys.stdout.flush()


