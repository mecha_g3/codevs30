//
//  AI.h
//  codevs30
//
//  Created by shingo on 2013/11/01.
//  Copyright (c) 2013年 shingo. All rights reserved.
//

#ifndef codevs30_AI_h
#define codevs30_AI_h

#include<vector>
#include<string>
#include<sstream>
#include<iostream>

using namespace std;

class Character{
public:
    int pid;
    int cid;
    int x;
    int y;
    int pow;
    int hav;
    int rem;
};

class Item{
public:
    int cid;
    int x;
    int y;
    int type;
};

class Bomb{
public:
    int cid;
    int x;
    int y;
    int pow;
    int turn;
    bool is_virtual;
    Bomb(){
        is_virtual = false;
    }
};

class Answer{
public:
    int dir;
    int delay;
    string to_string(){
        string dir_name[] = { "RIGHT", "DOWN", "LEFT", "UP", "NONE" };
        string ret = dir_name[dir];
        if(delay > 0){
            ret += " MAGIC " + std::to_string(long long(delay));
        }
        return ret;
    }
};

class AI{
public:
    bool _change_stage;
    int stage;
    int turn;
    int max_turn;
    int my_id;
    int H;
    int W;
    vector<string> field;
    vector<Character> m_chars;
    vector<Character> o_chars;
    vector<Item> items;
    vector<Bomb> bombs;

    vector<int> dx, dy;

    AI(){
        _change_stage = false;
        stage = 0;
        const int ax[] = { 1, 0,-1, 0, 0};
        const int ay[] = { 0, 1, 0,-1, 0};
        dx.resize(5);
        dy.resize(5);
        for(int i=0; i<5; ++i){
            dx[i] = ax[i];
            dy[i] = ay[i];
        }
    }
    
    bool is_change_stage(){
        return _change_stage;
    }

    bool input(string str){
        vector<string> before_field = field;
        field.clear();
        m_chars.clear();
        o_chars.clear();
        items.clear();
        bombs.clear();

        stringstream ss(str);

        ss >> turn >> max_turn >> my_id >> H >> W;
        
        _change_stage = false;
        for(int y = 0; y < H; y++){
            string line;
            ss >> line;
            for(int x = 0; x < W; ++x){
                if(line[x] == '+' && ! before_field.empty() && before_field[y][x] != '+'){
                    _change_stage = true;
                }
                if(line[x] == '@'){
                    line[x] = '.';
                }
            }
            field.push_back(line);
        }
        
        int n;
        ss >> n;
        while(n--){
            Character c;
            ss >> c.pid >> c.cid >> c.y >> c.x >> c.pow >> c.hav;
            c.rem = c.hav;

            if(c.pid == my_id){
                m_chars.push_back(c);
            }
            else{
                o_chars.push_back(c);
            }
        }

        ss >> n;
        while(n--){
            Bomb b;
            ss >> b.cid >> b.y >> b.x >> b.turn >> b.pow;
            bombs.push_back(b);
            for(int i = 0; i < m_chars.size(); ++i){
                if(m_chars[i].cid == b.cid){
                    m_chars[i].rem--;
                }
            }
            for(int i = 0; i < o_chars.size(); ++i){
                if(o_chars[i].cid == b.cid){
                    o_chars[i].rem--;
                }
            }
        }

        ss >> n;
        while(n--){
            Item it;
            string s;
            ss >> s;
            if(s == "NUMBER_UP"){
                it.type = 0;
            }
            else{
                it.type = 1;
            }
            ss >> it.y >> it.x;
            items.push_back(it);
        }

        string end;
        ss >> end;
        return end == "END";
    }
    
    virtual vector<Answer> solve() = 0;
};

#endif
