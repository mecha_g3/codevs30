//
//  FastAttackAI.cpp
//  codevs30
//
//  Created by shingo on 2013/11/01.
//  Copyright (c) 2013�N shingo. All rights reserved.
//

#include "FastAttackAI.h"

#include<iostream>
#include<vector>
#include<string>
#include<algorithm>
using namespace std;

FastAttackAI::FastAttackAI(){
  depth = 10;
  friend_ans = false;
  last_ans.resize(2);
}


vector<Answer> FastAttackAI::solve(){
    calc_firstok();

    for (int i = 0; i < o_chars.size(); i++) {
        const Character& chara = o_chars[i];
        for (int j = 0; j < 5; j++){
            const int y = chara.y + dy[j];
            const int x = chara.x + dx[j];
            if(firstok[y][x]){
                Bomb bomb;
                bomb.cid = chara.cid;
                bomb.y = y;
                bomb.x = x;
                bomb.turn = 5;
                bomb.pow = chara.pow;
                bomb.is_virtual = true;
                bombs.push_back(bomb);
            }
        }
    }

    vector<Answer> ret(m_chars.size());

    for (int i = 0; i < m_chars.size(); i++) {
        const Character& chara = m_chars[i];

        vector<vector<vector<int> > > check(5);
        int bomb_delay[] = {0, 5};

        for (int j = 0; j < 2; j++) {
            check[j] = getset(i, bomb_delay[j]);
        }

        vector<int> ar(10);
        for (int j = 0; j < ar.size(); j++){
            ar[j] = j;
        }

        random_shuffle(ar.begin(), ar.end());
       
        int best = 1;
        long long bestscore = -999999999999999999LL;

        for (int j = 0; j < ar.size(); j++) {
            int a = ar[j] / 5;
            int b = ar[j] % 5;
            if((a == 0) || (chara.rem > 0)){
                long long score = (check[a][b][0] - check[a][b][1]) * 10000000000000LL;
                if ((check[a][b][0] == check[a][b][1]) && (check[a][b][0] != depth)){
                    score -= 500000000LL;
                }

                score += (check[a][b][2]) * 10000000LL;

                int y = chara.y + dy[b];
                int x = chara.x + dx[b];
                
                for (int k = 0; k < items.size(); ++k) {
                    score += 50000LL / (abs(items[k].y - y) + abs(items[k].x - x) + 1);
                }
                
                for (int k = 0; k < o_chars.size(); ++k) {
                    score -= 5000LL * (abs(o_chars[k].y - y) + abs(o_chars[k].x - x));
                }
                
                for (int k = 0; k < m_chars.size(); ++k) {
                    int cy = m_chars[k].y;
                    int cx = m_chars[k].x;
                    if(friend_ans){
                        cy += dy[friend_move_index];
                        cx += dx[friend_move_index];
                    }
                    score -= 10000LL / (abs(y - cy) + abs(x - cx) + 1);
                }
                
                if(last_ans[i].dir != 4 && last_ans[i].dir == b){
                    score += 5000LL;
                }
                
                if (b == 4) {
                    score -= 10000LL;
                }
                
                if (a >= 1) {
                    if ((y % 2 == 1) && (x % 2 == 1) && (b != 4)) {
                        score += 100000LL;
                    } else {
                        score -= 100000LL;
                        if (last_ans[i].delay > 0) {
                            score -= 100000LL;
                        }
                    }
                }
                if (score > bestscore) {
                    bestscore = score;
                    best = ar[j];
                }
            }
        }

        Answer ans;
        ans.dir = best % 5;
        ans.delay = bomb_delay[best / 5];

        if(ans.delay > 0){
            Bomb bomb;
            bomb.cid = chara.cid;
            bomb.y = chara.y + dy[ans.dir];
            bomb.x = chara.x + dx[ans.dir];
            bomb.turn = ans.delay;
            bomb.pow = chara.pow;
            bomb.is_virtual = false;
            bombs.push_back(bomb);
            
            m_chars[i].rem--;
        }
        if(i==0){
            friend_ans = true;
            friend_move_index = ans.dir;
            friend_bomb_delay = ans.delay;
        }
        else{
            friend_ans = false;
        }
        ret[i] = ans;
    }

    last_ans = ret;
    return ret;
}
