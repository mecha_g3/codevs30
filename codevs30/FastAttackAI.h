//
//  FastAttackAI.h
//  codevs30
//
//  Created by shingo on 2013/11/01.
//  Copyright (c) 2013�N shingo. All rights reserved.
//

#ifndef codevs30_FastAttackAI_h
#define codevs30_FastAttackAI_h

#include"MechaAI.h"

class FastAttackAI : public MechaAI
{
public:
    FastAttackAI();
    virtual vector<Answer> solve();
};


#endif