#include <iostream>
#include <vector>
using namespace std;

int main()
{
    vector<int> v({ 9, 8, 7, 6, 5, 4, 3, 2, 1, 0 });
    for (auto x : v)
        cout << x << ' ';
    cout << endl;
}
