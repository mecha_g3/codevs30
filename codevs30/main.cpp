#include <iostream>
#include <vector>
#include <string>
#include <fstream>
#include "MechaAI.h"
#include "FastAttackAI.h"

using namespace std;


AI* create_ai(int stage){
  switch(stage%10){
  case 0:
    return new FastAttackAI();
  case 1:
    return new FastAttackAI();
  case 2:
    return new FastAttackAI();
  case 3:
    return new FastAttackAI();
  case 4:
    return new MechaAI();
  case 5:
    return new FastAttackAI();
  case 6:
    return new FastAttackAI();
  case 7:
    return new MechaAI();
  case 8:
    return new MechaAI();
  case 9:
    return new MechaAI();
  }
  return new MechaAI();
}

int main(int argc, char** argv)
{
    bool use_input_by_file = false;
    string path = "";

    if(argc > 1){
        for(int i=1; i<argc; i+=2){
            string cmd = argv[i];
            if(cmd == "-i"){
                if(i+1<argc){
                    path = argv[i+1];
                    use_input_by_file = true;
                }
                else{
                    cerr << "-i /path/to/file.txt" << endl;
                }
            }
        }
    }

    cout << "mecha" << endl;
    
    ifstream ifs;
    if(use_input_by_file){
        ifs.open(path);
    }

    int now_stage = 0;
    AI* ai = create_ai(now_stage);

    while (true) {
        string tmp, input;
        if(use_input_by_file){
            while(ifs && getline(ifs, tmp)){
                input += tmp + " ";
                if(tmp == "END")break;
            }
        }
        else{
            while(getline(cin, tmp)){
                input += tmp + " ";
                if(tmp == "END")break;
            }
        }

        if(! ai->input(input)){
          cerr << "Inpnut Error" << endl;
        }

        if(ai->is_change_stage()){
          cerr << "Stage" << now_stage << "clear." << endl;
          now_stage++;
          delete ai;
          ai = NULL;
          ai = create_ai(now_stage);
          if(! ai->input(input)){
            cerr << "Inpnut Error" << endl;
          }
        }

        vector<Answer> ans = ai->solve();
        cout << ans[0].to_string() << endl;
        cout << ans[1].to_string() << endl;
    }
    delete ai;
    return 0;
}