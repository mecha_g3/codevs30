#!/usr/bin/env python
# -*- coding: utf-8 -*-
import pygame
import sys
import os
import copy

from pygame.locals import *

CHAR_DELAY_FRAME = 8
TILE_SIZE = 40

# down = 0, right = 1, up = 2, left = 3
def calc_dir(before_x, before_y, after_x, after_y):
    if before_y < after_y:
	return 0
    if before_x < after_x:
	return 1
    if before_y > after_y:
	return 2
    if before_x > after_x:
	return 3
    return None

    
class ImageManager:
    def __init__(self):
	self._count = 0
	base_dir = os.path.dirname(__file__)
	self.white_bomber = pygame.image.load(os.path.join(base_dir, 'assets', 'white_bomber.png')).convert_alpha()
	self.black_bomber = pygame.image.load(os.path.join(base_dir, 'assets', 'black_bomber.png')).convert_alpha()
	self.red_bomber = pygame.image.load(os.path.join(base_dir, 'assets', 'red_bomber.png')).convert_alpha()
	self.blue_bomber = pygame.image.load(os.path.join(base_dir, 'assets', 'blue_bomber.png')).convert_alpha()
        self.item = pygame.image.load(os.path.join(base_dir, 'assets', 'item.png')).convert_alpha()
        self.bomb = pygame.image.load(os.path.join(base_dir, 'assets', 'bomb.png')).convert_alpha()
        self.block = pygame.image.load(os.path.join(base_dir, 'assets', 'block.png')).convert()
	self.font = pygame.font.SysFont(pygame.font.get_default_font(), 28)
    
    def get_hard_block(self):
	block = self.block.subsurface(Rect(0, 0, 40, 40))
        block = pygame.transform.smoothscale(block, (TILE_SIZE, TILE_SIZE))
	return block
    
    def get_soft_block(self):
	block = self.block.subsurface(Rect(0, 40, 40, 40))
        block = pygame.transform.smoothscale(block, (TILE_SIZE, TILE_SIZE))
	return block
    
    def get_white_bomber(self, dir, index):
	bomber = self.white_bomber.subsurface(Rect(index*20, dir*30, 20, 30))
        bomber = pygame.transform.smoothscale(bomber, (TILE_SIZE, TILE_SIZE+TILE_SIZE/2))
	return bomber

    def get_black_bomber(self, dir, index):
	bomber = self.black_bomber.subsurface(Rect(index*20, dir*30, 20, 30))
        bomber = pygame.transform.smoothscale(bomber, (TILE_SIZE, TILE_SIZE+TILE_SIZE/2))
	return bomber

    def get_red_bomber(self, dir, index):
	bomber = self.red_bomber.subsurface(Rect(index*20, dir*30, 20, 30))
        bomber = pygame.transform.smoothscale(bomber, (TILE_SIZE, TILE_SIZE+TILE_SIZE/2))
	return bomber

    def get_blue_bomber(self, dir, index):
	bomber = self.blue_bomber.subsurface(Rect(index*20, dir*30, 20, 30))
        bomber = pygame.transform.smoothscale(bomber, (TILE_SIZE, TILE_SIZE+TILE_SIZE/2))
	return bomber

    def get_item(self, type):
	item = None
	if type == 0:
	    item = self.item.subsurface(Rect(0, 16*((self._count/5)%2), 16, 16))
        else:
	    item = self.item.subsurface(Rect(16, 16*((self._count/5)%2), 16, 16))
        return pygame.transform.smoothscale(item, (TILE_SIZE, TILE_SIZE))

    def get_bomb(self, turn):
	bomb = self.bomb.subsurface(Rect(18*(turn%3), 0, 17, 19)).copy()
	bomb.blit(self.font.render(str(turn+1), True, (255,255,255)),(3, 0))
        return pygame.transform.smoothscale(bomb, (TILE_SIZE, TILE_SIZE))

	
    def update(self):
	self._count += 1
	
	
class Character:
    def __init__(self):
        self.pid = None # キャラクタのプレイヤーID
        self.id  = None # キャラクタのID
        self.y   = None # 位置（行）
        self.x   = None # 位置（列）
        self.pow = None # キャラクタの火力
        self.hav = None # 同時に出せる魔法陣数
	self.dir = None
	self.step= None
	self.delay = None

    def calc_display_info(self):
	dir, index, dx, dy = 0, 0, 0, 0
	
	if self.dir != None:
	    if self.delay == None:
		self.delay = CHAR_DELAY_FRAME
	    elif self.delay > 0:
		self.delay -= 1
	
	if self.dir != None:
	    dir = self.dir

	if self.delay != None:
	    index = 2 - self.delay/(CHAR_DELAY_FRAME/2)

	dx, dy = 0, 0
	if self.delay:
	    d = TILE_SIZE - (TILE_SIZE/CHAR_DELAY_FRAME) * (CHAR_DELAY_FRAME - self.delay)
	    move_scroll = [ (0,-d), (-d,0), (0,d), (d,0) ]
	    dx, dy = move_scroll[dir]
	return dx, dy, dir, index


class Bomb:
    def __init__(self):
        self.cid  = None # 魔法陣のキャラクタID
        self.y    = None # 位置（行）
        self.x    = None # 位置（列）
        self.turn = None # あと何ターンで発動するか
        self.pow  = None # 魔法陣の火力

class Item:
    def __init__(self):
        self.type = None # アイテムの種類 （0=NUMBER_UP, 1=POWER_UP）
        self.y    = None # 位置（行）
        self.x    = None # 位置（列）

class Game:
    def __init__(self):
        self.flash()
	
    def flash(self):
        self.turn = None
        self.MAX_TURN = None
        self.my_id = None
        self.W = None
        self.H = None
        self.char_size = None
        self.bomb_size = None
        self.item_size = None
	self.m_player1 = None
	self.m_player2 = None
	self.o_player1 = None
	self.o_player2 = None
	
        self.field = []
        self.bombs = []
        self.items = []
        self.wait = False

    def need_input(self):
        return not self.wait

    def input(self):
	self._before = None
	self._before = copy.deepcopy(self)
        self.flash()
	
        self.turn = input()
        self.MAX_TURN = input()
        self.my_id = input()
        self.H ,self.W = map(int, raw_input().split())
        
        for y in range(self.H):
            self.field.append(raw_input())
	
        self.char_size = input()
        for i in range(self.char_size):
            char = Character()
            char.pid, char.id, char.y ,char.x, char.pow, char.hav = map(int, raw_input().split())
	    if char.pid == self.my_id:
		if not self.m_player1:
		    self.m_player1 = char
		    if self._before and self._before.m_player1:
			last = self._before.m_player1
			char.dir = calc_dir(last.x, last.y, char.x, char.y)
		else:
		    self.m_player2 = char
		    if self._before and self._before.m_player2:
			last = self._before.m_player2
			char.dir = calc_dir(last.x, last.y, char.x, char.y)
	    else:
		if not self.o_player1:
		    if self._before and self._before.o_player1:
			last = self._before.o_player1
			char.dir = calc_dir(last.x, last.y, char.x, char.y)
		    self.o_player1 = char
		else:
		    self.o_player2 = char
		    if self._before and self._before.o_player2:
			last = self._before.o_player2
			char.dir = calc_dir(last.x, last.y, char.x, char.y)

        self.bomb_size = input()
        for i in range(self.bomb_size):
            bomb = Bomb()
            bomb.cid, bomb.y ,bomb.x, bomb.turn, bomb.pow = map(int, raw_input().split())
            self.bombs.append(bomb)
        
        self.item_size = input()
        for i in range(self.item_size):
            item = Item()
	    line = raw_input().split()
            if line[0] == 'NUMBER_UP':
                item.type = 0
            else:
                item.type = 1
            item.y = int(line[1])
            item.x = int(line[2])
            self.items.append(item)

        if raw_input() == "END":
	    self.wait = True
            return True
        else:
            return False


class App:
    def __init__(self):
        self._running = True
        self._display_surf = None
        self.size = self.weight, self.height = 800, 800
	
    def on_init(self):
        pygame.init()
        self._display_surf = pygame.display.set_mode(self.size, pygame.HWSURFACE | pygame.DOUBLEBUF)
        self._running = True
	self.image_manager = ImageManager()
        self.game = Game()
	print 'mecha'
	sys.stdout.flush()
 
    def on_event(self, event):
        if event.type == pygame.KEYDOWN:
	    ans = None

            if event.key == pygame.K_LEFT:
		ans = 'LEFT'
            elif event.key == pygame.K_RIGHT:
                ans = 'RIGHT'
            elif event.key == pygame.K_UP:
                ans = 'UP'
            elif event.key == pygame.K_DOWN:
                ans = 'DOWN'
            elif event.key == pygame.K_1:
                ans = 'NONE MAGIC 5'
            elif event.key == pygame.K_2:
                ans = 'NONE MAGIC 7'
            elif event.key == pygame.K_3:
                ans = 'NONE MAGIC 10'

	    elif event.key == pygame.K_ESCAPE:
		self._running = False
	    
	    if ans:
		move1 = ans
		move2 = "NONE"
		if self.game.m_player1.x == self.game.m_player2.x and self.game.m_player1.y == self.game.m_player2.y:
		    move2 = ans
		print move1
		print move2
		self.game.wait = False
	        sys.stdout.flush()

        if event.type == pygame.QUIT:
            self._running = False

    def on_loop(self):
	self.image_manager.update()
	if self.game.need_input():
	    self.game.input()

    def on_render(self):
	background = pygame.Surface(self._display_surf.get_size()).convert()
	background.fill((240, 240, 240))
	self._display_surf.blit(background, (0, 0))
	ground = pygame.Surface((TILE_SIZE, TILE_SIZE)).convert()
	ground.fill((32,100,64))
	fire = pygame.Surface((TILE_SIZE, TILE_SIZE)).convert()
	fire.fill((255, 0, 0))
	
	warning = pygame.Surface((TILE_SIZE, TILE_SIZE)).convert()
	warning.fill((128, 128, 0))
	
	for y in range(self.game.H):
	    for x in range(self.game.W):
		cell = self.game.field[y][x]
		self._display_surf.blit(ground, (x*TILE_SIZE, y*TILE_SIZE))
		if cell == "#":
		    self._display_surf.blit(self.image_manager.get_hard_block(), (x*TILE_SIZE, y*TILE_SIZE))
		elif cell == "+":
		    self._display_surf.blit(self.image_manager.get_soft_block(), (x*TILE_SIZE, y*TILE_SIZE))
		elif cell == "@":
		    self._display_surf.blit(fire, (x*TILE_SIZE, y*TILE_SIZE))
		    
	warning_map = [ [ None for x in range(self.game.W) ] for y in range(self.game.H) ]
	
        for bomb in self.game.bombs:
	    dir_list = [ (0,-1), (-1,0), (0,1), (1,0) ]
	    for dir in dir_list:
		for d in range(bomb.pow+1):
		    x = bomb.x + dir[0] * (d)
		    y = bomb.y + dir[1] * (d)
		    if x > 0 and x < self.game.W-1 and y > 0 and y < self.game.H-1 and self.game.field[y][x] == ".":
			if warning_map[y][x]:
			    warning_map[y][x] = min(warning_map[y][x], bomb.turn)
			else:
			    warning_map[y][x] = bomb.turn
		    else:
			break

	for y in range(self.game.H):
	    for x in range(self.game.W):
		if warning_map[y][x] != None:
		    warning.fill((128, 128, 0))
		    warning.blit(self.image_manager.font.render(str(warning_map[y][x]+1), True, (255,255,255)),(3, 0))
		    self._display_surf.blit(warning, (x*TILE_SIZE, y*TILE_SIZE))

        for bomb in self.game.bombs:
	    self._display_surf.blit(self.image_manager.get_bomb(bomb.turn), (bomb.x*TILE_SIZE, bomb.y*TILE_SIZE))
		    
	
        for item in self.game.items:
	    self._display_surf.blit(self.image_manager.get_item(item.type), (item.x*TILE_SIZE, item.y*TILE_SIZE))
	
	if self.game.m_player1:
	    char = self.game.m_player1
	    dx, dy, dir, index = char.calc_display_info()
	    self._display_surf.blit(self.image_manager.get_white_bomber(dir, index), (char.x*TILE_SIZE + dx, char.y*TILE_SIZE - TILE_SIZE/2 + dy))
	
	if self.game.m_player2:
	    char = self.game.m_player2
	    dx, dy, dir, index = char.calc_display_info()
	    self._display_surf.blit(self.image_manager.get_black_bomber(dir, index), (char.x*TILE_SIZE + dx, char.y*TILE_SIZE - TILE_SIZE/2 + dy))
	    
	if self.game.o_player1:
	    char = self.game.o_player1
	    dx, dy, dir, index = char.calc_display_info()
	    self._display_surf.blit(self.image_manager.get_red_bomber(dir, index), (char.x*TILE_SIZE + dx, char.y*TILE_SIZE - TILE_SIZE/2 + dy))
	    
	if self.game.o_player2:
	    char = self.game.o_player2
	    dx, dy, dir, index = char.calc_display_info()
	    self._display_surf.blit(self.image_manager.get_blue_bomber(dir, index), (char.x*TILE_SIZE + dx, char.y*TILE_SIZE - TILE_SIZE/2 + dy))
	    
	pygame.display.flip()

    def on_cleanup(self):
        pygame.quit()
 
    def on_execute(self):
        if self.on_init() == False:
            self._running = False
 
        while( self._running ):
            for event in pygame.event.get():
                self.on_event(event)
            self.on_loop()
            self.on_render()
        self.on_cleanup()
 
if __name__ == "__main__" :
    theApp = App()
    theApp.on_execute()