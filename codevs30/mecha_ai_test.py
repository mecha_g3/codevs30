#!/usr/bin/env python
# -*- coding: utf-8 -*-

import mecha_ai
import unittest
import sys
import os
import copy


org_stdin = sys.stdin

class MechaAITest(unittest.TestCase):

    def print_field(self, ai):

        print "------------------------------------------"
        print "Turn:" + str(ai.game.turn)
        field = copy.deepcopy(ai.game.field)
        for y in range(ai.game.H):
            line = field[y]
            for x in range(ai.game.W):
                for bomb in ai.game.bombs:
                    if bomb.x == x and bomb.y == y:
                        line = line[:x] + "B" + line[x+1:]

                for char in ai.game.m_chars:
                    if char.x == x and char.y == y:
                        if line[x] == "B":
                            line = line[:x] + "M" + line[x+1:]
                        else:
                            line = line[:x] + "m" + line[x+1:]

                for char in ai.game.o_chars:
                    if char.x == x and char.y == y:
                        if line[x] == "B":
                            line = line[:x] + "O" + line[x+1:]
                        elif line[x] == "m":
                            line = line[:x] + "s" + line[x+1:]
                        elif line[x] == "M":
                            line = line[:x] + "S" + line[x+1:]
                        else:
                            line = line[:x] + "o" + line[x+1:]
                            

            print line


    def test_get_name(self):
        ai = mecha_ai.MechaAI()
        self.assertEqual('mecha_g3', ai.get_name())

    def test_calc_feature_field_size(self):
        ai = mecha_ai.MechaAI()
        sys.stdin = open(os.path.join('testcase', '1.txt'), 'r')
        for _ in range(100):
            self.assertEqual(ai.input(), True)
            ai.calc_feature_field()
            self.assertEqual(len(ai.feature_field), 10)

    def test_calc_feature_field(self):
        ai = mecha_ai.MechaAI()
        sys.stdin = open(os.path.join('testcase', '2.txt'), 'r')

        self.assertEqual(ai.input(), True)
        self.assertEqual(ai.input(), True)
        self.assertEqual(ai.input(), True)
        self.assertEqual(ai.input(), True)
        self.assertEqual(ai.input(), True)
        self.assertEqual(ai.input(), True)

        self.assertEqual(ai.input(), True)
        ai.calc_feature_field()
        self.assertEqual(ai.calc_alive_turn(ai.game.m_chars[0]), 10)
        self.assertEqual(ai.calc_alive_turn(ai.game.m_chars[1]), 1)

        self.assertEqual(ai.input(), True)
        ai.calc_feature_field()
        self.assertEqual(ai.calc_alive_turn(ai.game.m_chars[0]), 10)
        self.assertEqual(ai.calc_alive_turn(ai.game.m_chars[1]), 0)

    def test_calc_feature_field(self):
        ai = mecha_ai.MechaAI()
        sys.stdin = open(os.path.join('testcase', '2.txt'), 'r')

        self.assertEqual(ai.input(), True)
        self.assertEqual(ai.input(), True)
        self.assertEqual(ai.input(), True)
        self.assertEqual(ai.input(), True)
        self.assertEqual(ai.input(), True)
        self.assertEqual(ai.input(), True)

        self.assertEqual(ai.input(), True)
        ai.calc_feature_field()
        self.assertEqual(ai.calc_alive_turn(ai.game.m_chars[0]), 10)
        self.assertEqual(ai.calc_alive_turn(ai.game.m_chars[1]), 1)

        self.assertEqual(ai.input(), True)
        ai.calc_feature_field()
        self.assertEqual(ai.calc_alive_turn(ai.game.m_chars[0]), 10)
        self.assertEqual(ai.calc_alive_turn(ai.game.m_chars[1]), 0)

    def test_aiuti(self):
        ai = mecha_ai.MechaAI()
        sys.stdin = open(os.path.join('testcase', '2.txt'), 'r')
        self.assertEqual(ai.input(), True)
        self.assertEqual(ai.input(), True)
        self.assertEqual(ai.input(), True)
        self.assertEqual(ai.input(), True)
        self.assertEqual(ai.input(), True)
        self.assertEqual(ai.input(), True)
        self.print_field(ai)
        ai.solve()



if __name__ == "__main__":
    unittest.main()
