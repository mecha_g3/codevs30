//
//  MechaAI.h
//  codevs30
//
//  Created by shingo on 2013/11/01.
//  Copyright (c) 2013年 shingo. All rights reserved.
//

#ifndef codevs30_MechaAI_h
#define codevs30_MechaAI_h

#include"AI.h"

class MechaAI : public AI
{
protected:
    int depth;
    bool friend_ans;
    int friend_move_index;
    int friend_bomb_delay;
    vector<Answer> last_ans;

    vector<vector<bool> > firstok;
    vector<vector<vector<int> > > feature_field;
    vector<int> feature_softblock_count;

    virtual void calc_firstok();
    virtual void calc_feature_field(bool enable_virtual_bomb);
    virtual int get_alive_turn(const Character& chara, int move_flag);
    virtual vector<vector<int> > getset(int p, int bomb_delay);
public:
    MechaAI();
    virtual vector<Answer> solve();
};


#endif
